
## Siz branchi nece yaradacaqsiniz?
branchi yaratmazdan qabaq:


    git pull origin main  



> (Eger code-da her hansisa bir deyishiklik olarsa feature yazdiginiz
> mudette, o zaman eyni bu shekilde cagiracaqsiniz)



## Yeni elaveler ucun (yeni xususiyetler ucun)

    git checkout -b "feature/sizin-branchiniz-adi" main  


yazaraq yeni branch yaradacaqsiniz.

daha sonra,

> (Eger code-da her hansisa bir deyishiklik olarsa feature yazdiginiz
> mudette, o zaman eyni bu shekilde cagiracaqsiniz)


    git push origin feature/sizin-branchiniz-adi  


daha sonra gitlab sehifesine girib **"merge requests"** hissesine gelib, create new request-e clickleyib source branche sizin feature branchiniz secib target branche ise main branchini secirsiniz  
daha sonra ise create/compare branches and continue basirsiniz.

her shey okaydise merge **conflict** yoxdursa merge basib hazir ki, code-a birleshdireceksiniz, yox her hansi bir **conflict** olarsa o zaman git pull origin main yazib conflictleri intellij ideada commit hissesinde resolve hissesine click edib size uygun shekilde yeni deyishikler ve sizin deyishiklerinizi birleshdirib apply eliyirsiniz daha sonra

    git commit -m "merge main"

yazib yeniden push etmeye cehd edirsiniz.